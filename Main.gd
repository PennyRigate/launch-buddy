extends Node2D
var transparentbg = true
export var current_monitor = 0
onready var testpos = $Position2D

func _ready():
	Engine.target_fps = 60
	get_viewport().transparent_bg = transparentbg

func _process(delta):
	#MAKE BACKGROUND TRANS
	if Input.is_action_just_pressed("debug_transparentbg"): 
		OS.request_attention()
		transparentbg = !transparentbg
		get_viewport().transparent_bg = transparentbg
