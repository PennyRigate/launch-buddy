extends Node2D
# ONREADYS
onready var sprite = $AnimatedSprite
onready var wander_timer = $WanderTimer

### RUNTIME VARS
var size = 3
## MOVEMENTS, the current path the buddy is moving in, regardless of state
enum Movement {IDLE,MOVE_TO_HOME,WANDER,TESTMARGINS}
var current_movement = Movement.WANDER
var velocity = Vector2(0,0)
var wander_target = OS.get_screen_size() / 2
var wander_speed = 4
	#amount to scale sockbugs movements (to account for different resolutions)
var speed_scale = 1
# WORKABLE AREA
export var margin_left = 0
export var margin_right = 0
export var margin_top = 0
export var margin_bottom = 0


func _ready():
	sprite.play("walk")
	var window_size = get_sprite_dimensions()
	OS.set_window_size(window_size)
	OS.set_window_position(OS.get_screen_size() / 2)
	
func _physics_process(delta):
	velocity = Vector2(0,0)
	#Stay on top left
	global_position = OS.get_window_size() / 2
	
	#MOVEMENTS
	match current_movement:
		Movement.IDLE:
			process_movement_idle()
			continue
		Movement.MOVE_TO_HOME:
			process_movement_movetohome()
			continue
		Movement.WANDER:
			process_movement_wander()
			continue
		Movement.TESTMARGINS:
			process_movement_testmargins()
			continue
	
	#APPLY MOVEMENT
	OS.set_window_position(OS.get_window_position()+velocity)
	
	# TEST DANCE
	if sprite.animation == "dance" && sprite.frame == 6: sprite.play("idle")

func _on_Area2D_input_event(viewport, event, shape_idx):
	if event.is_action_pressed("lclick"):
		sprite.play("dance")

# GET THE SIZE OF THE SPRITE IN PIXELS
func get_sprite_dimensions():
	var current_frame = sprite.get_frame()
	var current_anim = sprite.animation
	var texture_size = sprite.frames.get_frame(current_anim,current_frame).get_size()
	return texture_size * scale

#	MOVE TO POINT, moves the buddy to a specified point with a specified speeed,
#	can also return whether or not the buddy has reached the point yet
func move_to_point(target,speed):
	var pos = OS.get_window_position()
	var screen_size = OS.get_screen_size()
	#Stay in margins
	if target.x <= margin_left: target.x = margin_left + speed ## left margin
	if target.x >= screen_size.x - margin_right: target.x = (screen_size.x - margin_right) - speed ## right margin
	if target.y <= margin_top: target.y = margin_top + speed ## top margin
	if target.y >= screen_size.y - margin_bottom: target.y = (screen_size.y - margin_bottom) - speed ## bottom margin
	#Return move_to_point().is_at_desitination = true if destination is reached
	if pos.distance_to(target) > speed:
		velocity = pos.direction_to(target) * (speed * speed_scale)
		return {is_at_destination = false}
	else:
		return {is_at_destination = true}

		

func process_movement_idle():
	pass
	
func process_movement_movetohome():
	move_to_point(Vector2(1432,674),4)
		
func process_movement_wander():
	move_to_point(Vector2(wander_target),wander_speed)
	#print(move_to_point(Vector2(wander_target),wander_speed).is_at_destination)
	#Start move timer after reaching destination
	if move_to_point(Vector2(wander_target),wander_speed).is_at_destination == true:
		if wander_timer.is_stopped(): wander_timer.start()
		wander_timer.set_wait_time(rand_range(0.5,3))
		wander_timer.set_paused(false)
		sprite.play("idle")

func process_movement_testmargins():
	var pos = OS.get_window_position()
	move_to_point(get_owner().testpos.position,wander_speed)	

func _on_WanderTimer_timeout():
	#Find where to wander
	var screen_size = OS.get_screen_size()
	var pos = OS.get_window_position()
	var wander_distance = 300 * speed_scale
	wander_target = Vector2(rand_range(pos.x - wander_distance,pos.x + wander_distance),rand_range(pos.y - wander_distance,pos.y + wander_distance))
	wander_timer.set_paused(true)
	#Pause
	sprite.play("walk")
	if wander_target.x > pos.x: scale.x = size * 1
	if wander_target.x < pos.x: scale.x = size * -1
	
