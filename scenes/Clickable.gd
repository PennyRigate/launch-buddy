extends Node2D
onready var sprite = $Sprite
onready var polys = $Polygon2D
onready var timer = $Timer
export var speed = 0

	#Move the node (note, due to limitations if a node is moving too fast
	#it will move faster than the screen can update and there will be artifacts
	#this can be mitigated somewhat with a lower fps
	#i will improve on this later with code to make the fast moving nodes
	#update their position less smoothly, 
	
	#Nodes also cannot overlap so make code to keep them distanced
func _process(delta):
	position.y += (Input.get_axis("ui_up","ui_down") * speed) * delta
	position.x += (Input.get_axis("ui_left","ui_right") * speed) * delta
func get_polygon_points():
	#get the points of the polygon relative to the screen
	#rember! polys can only be 4 points (rectangle hitbox)
	var texture_size: Vector2 = sprite.texture.get_size() / 2
	var polygon_points: PoolVector2Array = [
		global_position + texture_size * Vector2(-1, -1),
		global_position + texture_size * Vector2(1, -1),
		global_position + texture_size * Vector2(1 , 1),
		global_position + texture_size * Vector2(-1 ,1)]
	return polygon_points

#Test click
func _on_Area2D_input_event(viewport, event, shape_idx):
	if event.is_action_pressed("lclick"):
		sprite.modulate = Color(0, 0.9375, 0.212402)
		timer.start()

func _on_Timer_timeout():
	sprite.modulate = Color(1, 1, 1)
